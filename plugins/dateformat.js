export default (context, inject) => 
{
    const dateFormat = (msg) => 
    {
        if(msg === undefined)return "";

        const d = new Date(msg);
        const ye = new Intl.DateTimeFormat('fr', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('fr', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('fr', { day: '2-digit' }).format(d);
        return(`${da} ${mo} ${ye}`);
    }
    

    

    
    // Inject $hello(msg) in Vue, context and store.
    inject('dateFormat', dateFormat)
    // For Nuxt <= 2.12, also add 👇
    context.$dateFormat = dateFormat
  }